#Run the results as Gecko codes. Confirmed to ONLY work on JPN GX.
#Tracks may repeat in cups. Tracks appearing are the 26 tracks playable in Time Attack (Ruby, Sapphire, Emerald, Diamond and AX + Sonic Oval), and Chapters 1, 3, 7, 8 and 9 from Story Mode.

#Origanal Source code is written by AugiteSoul.
#Edit it to GeckoCode by CH-MCL.

import random
 
tracklist=["01", "03", "05", "07", "08", "09", "0A", "0B", "0D", "0E", "0F", "10", "11", "15", "18", "19", "1A", "1B", "1C", "1D", "1F", "20", "21", "22", "23", "24", "25", "27", "2B", "2cC", "2D", "32"]
 
t1ruby=random.choice(tracklist)
t2ruby=random.choice(tracklist)
t3ruby=random.choice(tracklist)
t4ruby=random.choice(tracklist)
t5ruby=random.choice(tracklist)

#PAL
#ptr_cupData = 0x1A9B84;
#JPN
ptr_cupData = 0x1A5F5C;
ofs_cupPos = 0x84;

firstTrack = 0x01
#PAL
#line_t1t2 = 0xB058C
#JPN
line_t1t2 = 0xADD08
line_t3t4 = line_t1t2 + 0x4
line_t5t6 = line_t3t4 + 0x4


print("Ruby Cup")
print(t1ruby," ",t2ruby," ",t3ruby," ",t4ruby," ",t5ruby)
print("48000000 80"+"{:X}".format(ptr_cupData)+"\n"+
"38"+"{:06X}".format(line_t1t2)+" 000000"+"{:02X}".format(firstTrack)+"\n"+
"14"+"{:06X}".format(line_t1t2)+" 00"+t1ruby+"00"+t2ruby+"\n"+
"14"+"{:06X}".format(line_t3t4)+" 00"+t3ruby+"00"+t4ruby+"\n"+
"12"+"{:06X}".format(line_t5t6)+" 000000"+t5ruby+"\n"+
"14"+"{:06X}".format(line_t1t2+ofs_cupPos)+" 00"+t1ruby+"00"+t2ruby+"\n"+
"14"+"{:06X}".format(line_t3t4+ofs_cupPos)+" 00"+t3ruby+"00"+t4ruby+"\n"+
"12"+"{:06X}".format(line_t5t6+ofs_cupPos)+" 000000"+t5ruby+"\n"+
"E2000001 80008000\n"
)

firstTrack = 0x0E
line_t1t2 += 0xC
line_t3t4 += 0xC
line_t5t6 += 0xC

t1sapphire=random.choice(tracklist)
t2sapphire=random.choice(tracklist)
t3sapphire=random.choice(tracklist)
t4sapphire=random.choice(tracklist)
t5sapphire=random.choice(tracklist)
 
print("Sapphire Cup")
print(t1sapphire," ",t2sapphire," ",t3sapphire," ",t4sapphire," ",t5sapphire)
 
print("48000000 80"+"{:X}".format(ptr_cupData)+"\n"+
"38"+"{:06X}".format(line_t1t2)+" 000000"+"{:02X}".format(firstTrack)+"\n"+
"14"+"{:06X}".format(line_t1t2)+" 00"+t1sapphire+"00"+t2sapphire+"\n"+
"14"+"{:06X}".format(line_t3t4)+" 00"+t3sapphire+"00"+t4sapphire+"\n"+
"12"+"{:06X}".format(line_t5t6)+" 000000"+t5sapphire+"\n"+
"14"+"{:06X}".format(line_t1t2+ofs_cupPos)+" 00"+t1sapphire+"00"+t2sapphire+"\n"+
"14"+"{:06X}".format(line_t3t4+ofs_cupPos)+" 00"+t3sapphire+"00"+t4sapphire+"\n"+
"12"+"{:06X}".format(line_t5t6+ofs_cupPos)+" 000000"+t5sapphire+"\n"+
"E2000001 80008000\n"
)

firstTrack = 0x0F
line_t1t2 += 0xC
line_t3t4 += 0xC
line_t5t6 += 0xC

t1emerald=random.choice(tracklist)
t2emerald=random.choice(tracklist)
t3emerald=random.choice(tracklist)
t4emerald=random.choice(tracklist)
t5emerald=random.choice(tracklist)
 
print("Emerald Cup")
print(t1emerald," ",t2emerald," ",t3emerald," ",t4emerald," ",t5emerald)
 
print("48000000 80"+"{:X}".format(ptr_cupData)+"\n"+
"38"+"{:06X}".format(line_t1t2)+" 000000"+"{:02X}".format(firstTrack)+"\n"+
"14"+"{:06X}".format(line_t1t2)+" 00"+t1emerald+"00"+t2emerald+"\n"+
"14"+"{:06X}".format(line_t3t4)+" 00"+t3emerald+"00"+t4emerald+"\n"+
"12"+"{:06X}".format(line_t5t6)+" 000000"+t5emerald+"\n"+
"14"+"{:06X}".format(line_t1t2+ofs_cupPos)+" 00"+t1emerald+"00"+t2emerald+"\n"+
"14"+"{:06X}".format(line_t3t4+ofs_cupPos)+" 00"+t3emerald+"00"+t4emerald+"\n"+
"12"+"{:06X}".format(line_t5t6+ofs_cupPos)+" 000000"+t5emerald+"\n"+
"E2000001 80008000\n"
)

firstTrack = 0x18
line_t1t2 += 0xC
line_t3t4 += 0xC
line_t5t6 += 0xC

t1diamond=random.choice(tracklist)
t2diamond=random.choice(tracklist)
t3diamond=random.choice(tracklist)
t4diamond=random.choice(tracklist)
t5diamond=random.choice(tracklist)
 
print("Diamond Cup")
print(t1diamond," ",t2diamond," ",t3diamond," ",t4diamond," ",t5diamond)
 
print("48000000 80"+"{:X}".format(ptr_cupData)+"\n"+
"38"+"{:06X}".format(line_t1t2)+" 000000"+"{:02X}".format(firstTrack)+"\n"+
"14"+"{:06X}".format(line_t1t2)+" 00"+t1diamond+"00"+t2diamond+"\n"+
"14"+"{:06X}".format(line_t3t4)+" 00"+t3diamond+"00"+t4diamond+"\n"+
"12"+"{:06X}".format(line_t5t6)+" 000000"+t5diamond+"\n"+
"14"+"{:06X}".format(line_t1t2+ofs_cupPos)+" 00"+t1diamond+"00"+t2diamond+"\n"+
"14"+"{:06X}".format(line_t3t4+ofs_cupPos)+" 00"+t3diamond+"00"+t4diamond+"\n"+
"12"+"{:06X}".format(line_t5t6+ofs_cupPos)+" 000000"+t5diamond+"\n"+
"E2000001 80008000\n"
)

firstTrack = 0x1F
line_t1t2 += 0xC
line_t3t4 += 0xC
line_t5t6 += 0xC

t1ax=random.choice(tracklist)
t2ax=random.choice(tracklist)
t3ax=random.choice(tracklist)
t4ax=random.choice(tracklist)
t5ax=random.choice(tracklist)
t6ax=random.choice(tracklist)

print("AX Cup")
print(t1ax," ",t2ax," ",t3ax," ",t4ax," ",t5ax," ",t6ax)
 
print("48000000 80"+"{:X}".format(ptr_cupData)+"\n"+
"38"+"{:06X}".format(line_t1t2)+" 000000"+"{:02X}".format(firstTrack)+"\n"+
"14"+"{:06X}".format(line_t1t2)+" 00"+t1ax+"00"+t2ax+"\n"+
"14"+"{:06X}".format(line_t3t4)+" 00"+t3ax+"00"+t4ax+"\n"+
"14"+"{:06X}".format(line_t5t6)+" 00"+t5ax+"00"+t6ax+"\n"+
"14"+"{:06X}".format(line_t1t2+ofs_cupPos)+" 00"+t1ax+"00"+t2ax+"\n"+
"14"+"{:06X}".format(line_t3t4+ofs_cupPos)+" 00"+t3ax+"00"+t4ax+"\n"+
"14"+"{:06X}".format(line_t5t6+ofs_cupPos)+" 00"+t5ax+"00"+t6ax+"\n"+
"E2000001 80008000\n"
)
 
firstTrack = 0x24
line_t1t2 += 0xC
line_t3t4 += 0xC
line_t5t6 += 0xC

t1ac=random.choice(tracklist)
t2ac=random.choice(tracklist)
t3ac=random.choice(tracklist)
t4ac=random.choice(tracklist)
t5ac=random.choice(tracklist)
t6ac=random.choice(tracklist)
 
print("AC Cup - used for the hidden AX mode, most tracks are unplayable without expansive hacking")
print(t1ac," ",t2ac," ",t3ac," ",t4ac," ",t5ac," ",t6ac)
 
print("48000000 80"+"{:X}".format(ptr_cupData)+"\n"+
"38"+"{:06X}".format(line_t1t2)+" 000000"+"{:02X}".format(firstTrack)+"\n"+
"14"+"{:06X}".format(line_t1t2)+" 00"+t1ac+"00"+t2ac+"\n"+
"14"+"{:06X}".format(line_t3t4)+" 00"+t3ac+"00"+t4ac+"\n"+
"14"+"{:06X}".format(line_t5t6)+" 00"+t5ac+"00"+t6ac+"\n"+
"14"+"{:06X}".format(line_t1t2+ofs_cupPos)+" 00"+t1ac+"00"+t2ac+"\n"+
"14"+"{:06X}".format(line_t3t4+ofs_cupPos)+" 00"+t3ac+"00"+t4ac+"\n"+
"14"+"{:06X}".format(line_t5t6+ofs_cupPos)+" 00"+t5ac+"00"+t6ac+"\n"+
"E2000001 80008000\n"
)